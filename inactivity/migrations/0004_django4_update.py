# Generated by Django 4.0.3 on 2022-03-03 13:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("auth", "0012_alter_user_first_name_max_length"),
        ("authentication", "0020_userprofile_language_userprofile_night_mode"),
        ("inactivity", "0003_add_webhooks"),
    ]

    operations = [
        migrations.AlterField(
            model_name="inactivitypingconfig",
            name="groups",
            field=models.ManyToManyField(
                blank=True,
                help_text="Groups subject to the inactivity policy. If empty, applies to all groups.",
                related_name="+",
                to="auth.group",
            ),
        ),
        migrations.AlterField(
            model_name="inactivitypingconfig",
            name="states",
            field=models.ManyToManyField(
                blank=True,
                help_text="States subject to the inactivity policy. If empty, applies to all states.",
                related_name="+",
                to="authentication.state",
            ),
        ),
    ]
